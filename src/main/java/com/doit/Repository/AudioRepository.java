package com.doit.Repository;

import com.doit.Domain.AudioRecords;

import com.doit.Domain.Chapter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.sql.Blob;
import java.util.List;

/**
 * Created by sriya on 28/07/16.
 */
@Repository
public interface AudioRepository  extends JpaRepository<AudioRecords,Long>{
    @Query("select a from AudioRecords a where a.chapterId=?1")
    List<AudioRecords> findByChapterId(@Param("chapterId") Long chapterId);

    @Query("select a from  AudioRecords a where a.chapterId=?1 and a.userId=?2")
    List<AudioRecords> findByChapterIdAndUserId(@Param("chapterId") Long chapterId,@Param("userId") Long userId);

    @Query("select a from AudioRecords a where a.audioId=?1")
    List<AudioRecords> findAllByAudioId(@Param("audioId") Long audioId);

    @Modifying
    @Transactional
    @Query("delete  from AudioRecords a where a.audioId=?1")
    int deleteByAudioId(@Param("audioId") Long audioId);

    @Transactional
    @Modifying
    @Query(value = "insert into AUDIO_RECORDED(audioid,topicname) values(?1,?2)",nativeQuery = true)
    void insertUsr(@Param("audioid") Long audioid,@Param("topicname") String topicname);

    @Transactional
    @Modifying
    @Query(value = "insert into AUDIO_RECORDED(topicname) values(?1)",nativeQuery = true)
    void insert(@Param("topicname") String topicname);

    @Query(value = "select a from AudioRecords a where a.audioId <> ?1")
    List<AudioRecords> findAllOtherRecords(@Param("audioId") Long audioId);
    @Transactional
    @Modifying
    @Query(value = "update AudioRecords a set a.softDelete=1 where a.audioId=?1")
    void updateSoftDelete(@Param("audioId") Long audioId);
}
