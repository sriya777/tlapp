package com.doit.Repository;

import com.doit.Domain.AudioShared;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sriya on 02/08/16.
 */
@Repository
public interface ShareRepository extends JpaRepository<AudioShared,Long> {
    @Transactional
    @Modifying
    @Query(value = "insert into AUDIO_SHARED (audioid, sharedby, sharedTo) values(?1,?2,?3)", nativeQuery = true)
    void insertSharedDetails(@Param("audioId") Long audioId, @Param("sharedBy") Long sharedBy, @Param("sharedto") Long sharedTo);
}
