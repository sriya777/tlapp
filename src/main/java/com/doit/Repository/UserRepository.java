package com.doit.Repository;

import com.doit.Domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
   List<User> findAllByEmail(@Param("email") String email);
    @Query("select u,us from User u,Role r, UserStudent us where u.userId= ?1")
    List<User> findUserByUserId(@Param("userId") Long userId);
    User findByUsername(String username);
    User findByEmailAndPassword(String email,String password);
}
