package com.doit.Service;

import com.doit.Domain.User;
import com.doit.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sampa on 22-07-2016.
 */
@Service
public class UserServiceImpl  implements UseService{

    //private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserRepository userRepository;
    @Autowired
    public UserServiceImpl(UserRepository userRepository){
        this.userRepository=userRepository;
    }
    public Iterable<User> List(){
        return userRepository.findAll();
    }
    @Override
    public List<User> getEmail(String email) {
        return  userRepository.findAllByEmail(email);
    }

    @Override
    public List<User> findByUserId(Long userId) {
        return  userRepository.findUserByUserId(userId);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public User findByEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email,password);
    }
}
