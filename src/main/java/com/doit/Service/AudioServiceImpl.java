package com.doit.Service;

import com.doit.Domain.AudioRecords;
import com.doit.Repository.AudioRepository;
import com.doit.Repository.ShareRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.sql.Blob;
import java.util.List;

/**
 * Created by sriya on 28/07/16.
 */
@Service
public class AudioServiceImpl implements AudioService {
     private AudioRepository audioRepository;
    private ShareRepository shareRepository;
     @Autowired
     public AudioServiceImpl(AudioRepository audioRepository, ShareRepository shareRepository){
         this.audioRepository=audioRepository;
         this.shareRepository=shareRepository;
     }
    @Override
    public List<AudioRecords> findAllByAudioId(Long audioId) {
        return audioRepository.findAllByAudioId(audioId);
    }

    @Override
    public List<AudioRecords> findByChapterId(Long chapterId) {
        return audioRepository.findByChapterId(chapterId);
    }

    @Override
    public int deleteByAudioId(Long audioId) {
    return audioRepository.deleteByAudioId(audioId);
    }

    @Override
    public void insertUsr(Long audioid, String topicname) {
        audioRepository.insertUsr(audioid,topicname);
    }
    @Override
    public void insert( String topicname) {
        audioRepository.insert(topicname);
    }

    @Override
    public void insertSharedDetails(Long audioId, Long sharedBy, Long sharedTo) {
        shareRepository.insertSharedDetails(audioId,sharedBy,sharedTo);
    }

    @Override
    public List<AudioRecords> findAllOtherRecords(@Param("audioId") Long audioId) {
        return audioRepository.findAllOtherRecords(audioId);
    }

    @Override
    public void updateSoftDelete(Long audioId) {
        audioRepository.updateSoftDelete(audioId);
    }

    @Override
    public List<AudioRecords> findByChapterIdAndUserId(Long chapterId, Long userId) {
        return audioRepository.findByChapterIdAndUserId(chapterId,userId);
    }

}
