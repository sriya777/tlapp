package com.doit.Service;

import com.doit.Domain.AudioRecords;
import com.doit.Domain.Chapter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by sriya on 25/07/16.
 */
public interface TopicsService {
    List<Chapter> findByChapterId(Long chapterId);
    List<Chapter> findBySubjectId(Long subjectId);

}
