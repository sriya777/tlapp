package com.doit.Service;


import com.doit.Domain.Role;
import com.doit.Domain.User;
import com.doit.Repository.UserDetailsRepository;
import com.doit.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by sampa on 24-07-2016.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    public List<User> findAllOtherStudents(Long userId) {
        return userDetailsRepository.findAllOtherStudents(userId);
    }

}
