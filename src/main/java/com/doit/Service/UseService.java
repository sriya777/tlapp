package com.doit.Service;


import com.doit.Domain.User;

import java.util.List;

/**
 * Created by sampa on 22-07-2016.
 */
public interface UseService {
  List<User> getEmail(String email);
  List<User> findByUserId(Long userId);
  User findByUsername(String username);
  User findByEmailAndPassword(String email,String password);

}
