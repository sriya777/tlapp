package com.doit.Service;

import com.doit.Domain.AudioRecords;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.sql.Blob;
import java.util.List;

/**
 * Created by sriya on 28/07/16.
 */
@Service
public interface AudioService {
    List<AudioRecords> findAllByAudioId(Long audioId);
    List<AudioRecords> findByChapterId(Long chapterId);
    int deleteByAudioId(Long audioId);
    void insertUsr(Long audioid, String topicname);
    void insert( String topicname);
    void insertSharedDetails(Long audioId, Long sharedBy, Long sharedTo);
    List<AudioRecords> findAllOtherRecords(@Param("audioId") Long audioId);
    void updateSoftDelete(Long audioId);
    List<AudioRecords> findByChapterIdAndUserId(Long chapterId, Long userId );
}
