//package com.doit.aspect;
//
//import org.hibernate.SessionFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//
///**
// * Created by sriya on 08/08/16.
// */
//
//public class WebSession {
//
//    private SessionFactory sessionFactory;
//    @Autowired
//    public WebSession(SessionFactory sessionFactory){
//        this.sessionFactory=sessionFactory;
//    }
//
//    public SessionFactory getSessionFactory() {
//        return sessionFactory;
//    }
//
//    public void setSessionFactory(SessionFactory sessionFactory) {
//        this.sessionFactory = sessionFactory;
//    }
//}
