package com.doit.Domain;

import javax.persistence.*;

/**
 * Created by sampa on 24-07-2016.
 */
@Entity
@Table(name = "user_students")
public class UserStudent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "studentid")
    private long studentId;
    @Column(name = "role_id")
    private long id;
    @Column(name = "parentid")
    private long patentId;
    @Column(name = "classid")
    private long classId;
    @Column(name = "schoolid")
    private long schoolId;
    @Column(name = "firstname")
    private String firstName;
    @Column(name = "lastname")
    private  String lastName;
    @Column(name = "gender")
    private String gender;


    public UserStudent(){}
    public UserStudent(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UserStudent(long id, long patentId, long classId, long schoolId, String lastName, String firstName, String gender) {
        this.id = id;
        this.patentId = patentId;
        this.classId = classId;
        this.schoolId = schoolId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.gender = gender;
    }

    public long getPatentId() {
        return patentId;
    }

    public void setPatentId(long patentId) {
        this.patentId = patentId;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public long getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(long schoolId) {
        this.schoolId = schoolId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
