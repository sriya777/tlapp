package com.doit.Domain;

import javax.persistence.*;

/**
 * Created by sriya on 25/07/16.
 */
@Entity
@Table(name = "AUDIO_SHARED")
public class AudioShared {
    @Column(name = "sharedby")
    private Long sharedBy;
    @Column(name="sharedto")
    private  Long sharedTo;
    @Id
    @Column(name = "audioid")
    private  Long audioId;
    public AudioShared(){}

    public Long getSharedBy() {
        return sharedBy;
    }

    public void setSharedBy(Long sharedBy) {
        this.sharedBy = sharedBy;
    }

    public Long getSharedTo() {
        return sharedTo;
    }

    public void setSharedTo(Long sharedTo) {
        this.sharedTo = sharedTo;
    }

    public Long getAudioId() {
        return audioId;
    }

    public void setAudioId(Long audioId) {
        this.audioId = audioId;
    }
}
