package com.doit.Domain;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by sampa on 24-07-2016.
 */
@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "roleid")
    private Long id;
    @Column(name = "tablename")
    private String tableName;


    public Role(){}
    public Role(Long id,String tableName) {
        this.id=id;
        this.tableName = tableName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return tableName;
    }

    public void setName(String tableName) {
        this.tableName = tableName;
    }

}
