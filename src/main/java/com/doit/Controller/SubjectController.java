package com.doit.Controller;

import com.doit.Domain.Subjects;
import com.doit.Domain.User;
import com.doit.Repository.SubjectRepository;
import com.doit.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.List;

/**
 * Created by sriya on 25/07/16.
 */
@RestController
@RequestMapping("/dashboard")
public class SubjectController {

    private SubjectRepository subjectRepository;
    private StudentService studentService;

    @Autowired
    public SubjectController(SubjectRepository subjectRepository, StudentService studentService){
        this.subjectRepository=subjectRepository;
        this.studentService=studentService;
    }

    /**
    * Getting one subject details for the dashboard page based on subjectId
    * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/sub/{subjectId}", method = RequestMethod.GET)
    public  List<Subjects> getById(@PathVariable(value = "subjectId") Long subjectId){
        return  studentService.findBySubjectId(subjectId);
    }
    /**
    * Getting all the subjects for dashboard page based on classId
    * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/subjects/{classId}", method = RequestMethod.GET)
    public  List<Subjects> getByChapId(@PathVariable(value = "classId") Long classId){
        return  studentService.findByClassId(classId);
    }

}
