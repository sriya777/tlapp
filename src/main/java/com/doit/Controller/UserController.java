package com.doit.Controller;

import com.doit.Domain.User;
import com.doit.Exceptions.HandleException;
import com.doit.Repository.UserDetailsRepository;
import com.doit.Repository.UserRepository;
import com.doit.Service.UseService;

import com.doit.Service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.CrossOrigin;


/**
 * Created by sampa on 22-07-2016.
 */
@RestController
@RequestMapping("/dashboard")
public class UserController {

    private UserRepository userRepository;
    private UserDetailsRepository userDetailsRepository;
    private UseService useService;
    private UserDetailsService userDetailsService;
    @Autowired
    public UserController(UseService useService, UserRepository userRepository, UserDetailsRepository userDetailsRepository,UserDetailsService userDetailsService){
        this.useService=useService;
        this.userRepository=userRepository;
        this.userDetailsRepository=userDetailsRepository;
        this.userDetailsService= userDetailsService;
    }

    /**
     * Getting list of all users based on email
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/byEmail/{email}", method = RequestMethod.GET)
    public List<User> getByEmail(@PathVariable(value = "email") String email){
        return  useService.getEmail(email);
    }


    /**
     * Getting list of all users based on userId
     * **/
    @RequestMapping(value = "/byId/{userId}", method = RequestMethod.GET)
    public  List<User> getByUserId(@PathVariable(value = "userId") Long userId){
        return  useService.findByUserId(userId);
    }

    /**
     * Getting list of all users from users_all table
     * **/
    @RequestMapping(value ="/all", method = RequestMethod.GET)
    public Iterable<User> getUsers(){
        return userRepository.findAll();
    }



    /**
     * Getting a user based on username
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/byUser/{username}", method = RequestMethod.POST,consumes = "application/json", produces = "application/json")
    public @ResponseBody User getByUsername(@RequestParam String username){
        User user= useService.findByUsername(username);
       return user;
    }

    /**
     * Getting a valid user
     * **/

    @RequestMapping(value="/usrchk" ,method = RequestMethod.POST)
    public @ResponseBody  User getByEmailAndPassword(@RequestParam String email,@RequestParam String password){
        User user=useService.findByEmailAndPassword(email,password);
        if(user.getEmail().equals(email) && user.getPassword().equals(password) && user.getEmail()!=null && user.getPassword()!=null)
            return  user;
        else throw new RuntimeException();
    }

    /**
     * Getting list of all respective usernames and passwords
     * **/
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public List<User> get(){
        return userDetailsRepository.findAll();
    }

    /**
     * Getting list of all users based on username
     * **/
    @RequestMapping(value = "/pall",method = RequestMethod.POST)
    public @ResponseBody  User posting(@RequestParam String username){
        User user= useService.findByUsername(username);
        return user;
    }

    /**
     * Getting list of all shared users based on userId
     * **/
    @RequestMapping(value = "/share/{userId}",method = RequestMethod.GET)
    List<User> getAllOtherStudents(@PathVariable("userId") Long userId){
    return userDetailsService.findAllOtherStudents(userId);
    }
}
