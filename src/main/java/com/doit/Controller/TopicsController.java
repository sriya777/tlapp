package com.doit.Controller;

import com.doit.Domain.AudioRecords;
import com.doit.Domain.Chapter;
import com.doit.Domain.Subjects;
import com.doit.Repository.ChapterRepository;
import com.doit.Service.TopicsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import java.util.List;

/**
 * Created by sriya on 25/07/16.
 */
@RestController
@RequestMapping("/dashboard")
public class TopicsController {
    private ChapterRepository chapterRepository;
    private TopicsService topicsService;
    @Autowired
    public TopicsController(ChapterRepository chapterRepository,TopicsService topicsService){
    this.chapterRepository=chapterRepository;
        this.topicsService=topicsService;
    }

    /**
     * Getting list of all subjects based on chapterId
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/sub/{chapterId}", method = RequestMethod.GET)
    public List<Chapter> getByChapterId(@PathVariable(value = "chapterId") Long chapterId){
        return topicsService.findByChapterId(chapterId);
    }


    /**
     * Getting all the chapter details of a particular subject based on chapterId
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/chapters/{subjectId}", method = RequestMethod.GET)
    public List<Chapter> getByStudentId(@PathVariable(value = "subjectId") Long subjectId){
        return topicsService.findBySubjectId(subjectId);
    }


}
