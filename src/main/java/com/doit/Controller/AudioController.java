package com.doit.Controller;


import com.doit.Domain.AudioRecords;
import com.doit.Domain.User;
import com.doit.Repository.AudioRepository;
import com.doit.Repository.ShareRepository;
import com.doit.Service.AudioService;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.sql.Blob;
import java.util.List;
/**
 * Created by sriya on 28/07/16.
 */
@RestController
@RequestMapping(value = "/dashboard")
public class AudioController {

    private AudioRepository audioRepository;
    private AudioService audioService;
    private ShareRepository shareRepository;
    @Autowired
    public AudioController(AudioService audioService,AudioRepository audioRepository, ShareRepository shareRepository){
        this.audioRepository=audioRepository;
        this.audioService=audioService;
        this.shareRepository=shareRepository;
    }

    /**
    * Getting all the audios based on audioId
    * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/audios/{audioId}",method = RequestMethod.GET)
    public List<AudioRecords> getByAudioId(@PathVariable(value = "audioId") Long audioId){
        return audioService.findAllByAudioId(audioId);
    }

    /**
     * Just For checking :)
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/audios/1",method = RequestMethod.POST)
    public void getByAId(@PathVariable("topicName") String topicName){
        Logger logger= LoggerFactory.getLogger(AudioController.class);
        logger.info("Inside save"+topicName);
        System.out.println(topicName);
    }

    /**
    * Getting all the audio files for that particular chapter based on chapterid
    * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getAudio/{chapterId}", method = RequestMethod.GET)
    public List<AudioRecords> getByChapId(@PathVariable(value = "chapterId") Long chapterId){
        return  audioService.findByChapterId(chapterId);
    }

    /**
     * Getting all the audio files based on userid and chapterid
     * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getAudios/{chapterId}/withuser/{userId}", method = RequestMethod.GET)
    public List<AudioRecords> byChapterAndUserId(@PathVariable Long chapterId, @PathVariable Long userId){
        return audioService.findByChapterIdAndUserId(chapterId,userId);
    }

    /**
    *Deleting an audio recording based on autioId
    * **/
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/delAudio/{audioId}",method = RequestMethod.DELETE)
    public int deleteByAudioId(@PathVariable(value = "audioId") Long audioId){
        System.out.println(audioId);
        Logger logger= LoggerFactory.getLogger(AudioController.class);
        logger.info("Inside Delete");
        if(audioId!=null){
            return audioRepository.deleteByAudioId(audioId);
        }
        return 0;
    }

    /**
     *Saving an audio recording based on autioId and topicname
     * **/
    @RequestMapping(value = "/save/",method = RequestMethod.POST)
   public  void saveAudiosbyAudioId(@RequestParam(value = "audioid") Long audioid, @RequestParam("topicname") String topicname){
    audioService.insertUsr(audioid,topicname);
   }


    /**
     *Saving an audio recording based on topicname
     * **/
    @RequestMapping(value = "/seva/{topicname}",method = RequestMethod.POST)
    public void saveAudiosByTopicName(@PathVariable("topicname") String topicname){
        audioService.insert(topicname);
    }


    /**
     *Saving an audio recording into shared table
     * **/
    @RequestMapping(value = "/shareit",method = RequestMethod.POST)
    public void shareAudio(@RequestParam("audioId") Long audioId, @RequestParam("sharedBy") Long sharedBy, @RequestParam("sharedTo") Long sharedTo){
        audioService.insertSharedDetails(audioId,sharedBy,sharedTo);
     }


    /**
     *Saving a recorded audio file for that chapter
     * **/
    @RequestMapping(value = "/ins/",method = RequestMethod.PUT)
    public void insertAudioSharedRecords(@RequestBody AudioRecords audioRecords) {
        AudioRecords insert=new AudioRecords();
        insert.setTopicName(audioRecords.getTopicName());
        insert.setComments(audioRecords.getComments());
        insert.setReference(audioRecords.getReference());
        insert.setChapterId(audioRecords.getChapterId());
        insert.setUserId(audioRecords.getUserId());
        insert.setSource(audioRecords.getSource());
        insert.setUrl("audio_recording_mainAudiomp3.mp3");
        insert.setSoftDelete(0);
        audioRepository.save(insert);
    }


    /**
     *Removing a record from the player based on audioId
     * **/
    @RequestMapping(value = "/removeRecord/{audioId}", method = RequestMethod.GET)
    public List<AudioRecords> getAllOtherRecords(@PathVariable("audioId") Long audioId){
        audioService.updateSoftDelete(audioId);
        return audioService.findAllOtherRecords(audioId);
    }



    /**
     *Saving shared record details into the respective users playlist based on userId
     * **/
    @RequestMapping(value = "/shared/", method=RequestMethod.PUT)
    public void insertSharedRecords(@RequestBody AudioRecords audioRecords){
        AudioRecords insert=new AudioRecords();
        insert.setChapterId(audioRecords.getChapterId());
        insert.setTopicName(audioRecords.getTopicName());
        insert.setComments(audioRecords.getComments());
        insert.setReference(audioRecords.getReference());
        insert.setSource(audioRecords.getSource());
        insert.setUserId(audioRecords.getUserId());
        insert.setLength(audioRecords.getLength());
        insert.setListenCount(audioRecords.getListenCount());
        insert.setSoftDelete(0);
        audioRepository.save(insert);
    }

}
